$(document).ready(function() {
  $(".project-head").addClass("project-head-animate");
  $(".project-content").addClass("project-content-animate");
});


$(document).ready(function() {
  $(".whats-new").addClass("whats-new-animate");
  $(".header-wrap").addClass("header-animate");
  $(".header-art").addClass("header-art-animate");

  if ( /Android|iPhone|iPad|iPod|Opera Mini/i.test(navigator.userAgent) == false ) {

    var intro = $("#intro").position().top;
    var whatsnew = $("#whatsnew").position().top - 50
    var projects = $("#projects").position().top - 50
    var contact = $("#contact").position().top - 440
    calculate()
    $(window).scroll( function(){
      calculate()
    })
    function calculate() {
      if ($(window).scrollTop() < whatsnew) {
        updateClass("a#dot-intro")
      } else if ($(window).scrollTop() < projects){
        updateClass("a#dot-whatsnew")
      } else if ($(window).scrollTop() < contact){
        updateClass("a#dot-projects")
      } else {
        updateClass("a#dot-contact")
      }
    }
    function updateClass(id) {
      $(".dotnav a").removeClass("active");
      $(id).addClass("active");
    }

    $(".dotnav a").on('click',function(e){
      e.preventDefault();
      var scroll = $(this).data("scroll")
      $('html, body').animate({
        scrollTop: $("#"+scroll+"").position().top
      }, 400);
    })

  }
});



